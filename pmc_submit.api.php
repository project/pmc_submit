<?php
/**
 * @file
 * Module to submit mauscripts to PubMed.
 *
 * Date: 15 mar 2018 12:48:57
 * File: pmc_submit.api.php
 * Author: stefano.
 */

/**
 * Volume and Issue.
 *
 * SC 20 mar 2018 12:21:44 stefano.
 *
 * @param $node object
 *   The node object.
 *
 * @return array
 *   A multidimensional array whith key
 *   - journal => An array with keys
 *
 *       - issue => The issue number, e.g., 4 Suppl 2, 6 Pt 2, 7-8, etc. This
 *                  tag is Required if the Volume tag is not present [string]
 *       - volume => The volume name or number of the journal, including any.
 *                   supplement information, e.g., 12 Suppl 2, 514 (Pt 2), 19
 *                   Suppl , etc. This tag is Required if the Issue tag is not
 *                   present [string].
 */
function hook_pmc_submit_journal_vol($node) {
  return array( 'journal' => array(
    'issue' => '4 Suppl 2',
    'volume' => '514 (Pt 2)',
  ));
}

/**
 * DOI manuscript id.
 *
 * SC 22 mar 2018 11:59:09 stefano.
 *
 * @param $node object
 *   The node object.
 *
 * @return array
 *   An array whith key
 *   - doi => The Digital Object Identifier, a number assigned by an
 *            international organization. The DOI System is a system for
 *            identifying and exchanging intellectual property in the digital
 *            environment. DOIs are issued to registrants by the DOI
 *            Registration Agency. More information about this standardized
 *            format can be obtained at http://www.doi.org/ [string].
 *
 */
function hook_pmc_submit_doi($node) {
  return array('doi' => '10.1309/AJCP4D7RXOBHLKGJ');
}

/**
 * PII manuscript id.
 *
 * SC 22 mar 2018 15:54:45 stefano.
 *
 * @param $node object
 *   The node object.
 *
 * @return array
 *   An array whith key
 *   - pii => Publisher Item Identifier, any internal reference identifier used
 *            in the publishing process. This identifier is assigned by the
 *            publisher [string].
 */
function hook_pmc_submit_pii($node) {
  return array('pii' => 'j143/3/336');
}

/**
 * The list of the article authors.
 *
 * SC 29 mar 2018 16:06:56 stefano.
 *
 * @param $node object
 *   The node object.
 *
 * @return array
 *   A multidimensional array with key
 *   - authors => Some multidimensional array of information about each Author:
 *     - firstname => An array with keys
 *       - value => The Author’s full first name is required if it appears in
 *                  the print or online version of the journal. First initial
 *                  is acceptable if full name is not available. This tag is
 *                  Required if the LastName tag is present [string].
 *       - empty => If represent a Single Personal Author Name [bool].
 *
 *     - middlename => The Author’s full middle name, or initial if the full
 *                     name is not available. Multiple names are allowed in
 *                     this tag [string].
 *     - lastname => The Author’s last name. This tag is Required if the
 *                   FirstName tag is present [string].
 *     - suffix => The Author's suffix, if any, e.g. "Jr", "Sr", "II", "IV". Do
 *                 not include honorific titles, e.g. "M.D.", "Ph.D." [string].
 *     - collectivename => The name of the authoring committee or organization.
 *                         CollectiveName can be used instead of or in addition
 *                         to a personal name [string]. This can replace all
 *                         above fields.
 *     - affiliation => Some array of data with keys
 *       - value => The institution(s) that the author is affiliated with.
 *                  Include the following data, if available: division of the
 *                  institution, institution name, city, state, postal or zip
 *                  code, country, e-mail address. Do not include the word
 *                  'e-mail' [string].
 *       - identifier => Some array of data with keys
 *         - value => The institution(s) ID [string].
 *         - source => Designates the organizational authority that established
 *                     the unique identifier, e.g., ORCID [string].
 *
 *     - identifier => Some array of data with keys
 *       - value => An author (personal or collective) or investigator’s
 *                  author ID [string].
 *       - source => Designates the organizational authority that established
 *                   the unique identifier, e.g., ORCID [string].
 *     - equalcontrib => To indicate equal contribution among authors [bool].
 *
 *
 */
function hook_pmc_submit_authors($node) {
  return array(
    'authors' => array(
      array(
        'firstname' => array(
          'value' => 'Autore',
          'empty' => 'N',
        ),
        'middlename' => '1',
        'lastname' => 'Uno',
        'suffix' => 'Jr',
        'collectivename' => 'Test 1',
        'affiliation' => array(
          array(
            'value' => 'affiliation 1',
            'identifier' => array(
              array(
                'value' => 'aff 1 ident 1',
                'source' => 'validator 1',
              ),
            ),
          ),
        ),
        'identifier' => array(
          array(
            'value' => 'identifier 1',
            'source' => 'ident valid 1',
          ),
        ),
        'equalcontrib' => FALSE,
      ),
    ),
  );
}

/**
 * The list of the group of authors.
 *
 * SC 30 mar 2018 19:06:50 stefano.
 *
 * @param $node object
 *   The node object.
 *
 * @return array
 *   A multidimensional array with key
 *   - groups => Some multidimensional array of information about each Group of
 *               author:
 *     - groupname => The name of the authoring committee or organization.
 *     - individualname => Some multidimensional array of information about the
 *                         name of individual members belonging to the authoring
 *                         committee or organization.
 *       - firstname => The Author’s full first name is required if it appears
 *                      in the print or online version of the journal. First
 *                      initial is acceptable if full name is not available.
 *                      This tag is Required if the LastName tag is present
 *                      [string].
 *       - middlename => The Author’s full middle name, or initial if the full
 *                       name is not available. Multiple names are allowed in
 *                       this tag [string].
 *       - lastname => The Author’s last name. This tag is Required if the
 *                     FirstName tag is present [string].
 *       - suffix => The Author's suffix, if any, e.g. "Jr", "Sr", "II", "IV".
 *                   Do not include honorific titles, e.g. "M.D.", "Ph.D."
 *                   [string].
 *       - affiliation => Some array of data with keys
 *         - value => The institution(s) that the author is affiliated with.
 *                    Include the following data, if available: division of
 *                    the institution, institution name, city, state, postal
 *                    or zip code, country, e-mail address. Do not include
 *                    the word 'e-mail' [string].
 *         - identifier => Some array of data with keys
 *           - value => The institution(s) ID [string].
 *           - source => Designates the organizational authority that
 *                       established the unique identifier, e.g., ORCID
 *                       [string].
 *
 *       - identifier => Some array of data with keys
 *         - value => An author (personal or collective) or investigator’s
 *                    author ID [string].
 *         - source => Designates the organizational authority that established
 *                     the unique identifier, e.g., ORCID [string].
 *
 *
 */
function hook_pmc_submit_groups($node) {
  return array(
    'groups' => array(
      array(
        'groupname' => 'gruppo 1',
        'individualname' => array(
          array(
            'firstname' => 'Autore',
            'middlename' => '1',
            'lastname' => 'Uno',
            'suffix' => 'Jr',
            'affiliation' => array(
              array(
                'value' => 'affiliation 1',
                'identifier' => array(
                  array(
                    'value' => 'aff 1 ident 1',
                    'source' => 'validator 1',
                  ),
                ),
              ),
            ),
            'identifier' => array(
              array(
                'value' => 'identifier 1',
                'source' => 'ident valid 1',
              ),
            ),
          ),
        ),
      ),
    ),
  );
}

/**
 * Used to identify the type of article.
 *
 * SC 03 apr 2018 18:54:50 stefano.
 *
 * @param $node object
 *   The node object.
 *
 * @return array
 *   A multidimensional array with key
 *   - publicationtype => An array with the types of article.
 *
 */
function hook_pmc_submit_publicationtype($node) {
  return array(
    'publicationtype' => array('Addresses', 'Journal Article',),
  );
}

/**
 * Article ID.
 *
 * SC 03 apr 2018 19:21:15 stefano.
 *
 * @param $node object
 *   The node object.
 *
 * @return array
 *   A multidimensional array with key
 *   - articleid => Some multidimensional array of information about the types
 *                  of the article:
 *     - value => The Article Identifier [string]
 *     - idtype => Arrtibute which may include only one of the following values
 *                 for each identifier:
 *                  * pii
 *                  * doi
 *                  * pubmed
 *                 (see the doc for the full list).
 *
 */
function hook_pmc_submit_articleid($node) {
  return array(
    'articleid' => array(
      array(
        'value' => _piccinbp_doi_get_doi($node->nid)),
        'idtype' => 'doi',
      ),
    ),
  );
}

/**
 * History.
 *
 * SC 04 apr 2018 09:50:14 stefano.
 *
 * @param $node object
 *   The node object.
 *
 * @return array
 *   A multidimensional array with key
 *   - history => Some multidimensional array of information about the history
 *                of the article:
 *     - pubdate => An array who identify a date.
 *       - year => The 4-digit year of publication, REQUIRED. [int]
 *       - month =>  This tag may only contain the numbers 1-12, the month (in
 *                   English) or the first three letters of the English months.
 *                   [string]
 *       - season => The season of publication (do not use if a Month is
 *                   available); ex: Winter, Spring, Summer, Fall. [string]
 *       - day => The day of publication. This tag may only contain the numbers
 *                1-31. [int]
 *
 *     - pubstatus => The PubStatus attribute (REQUIRED), which may contain only
 *                    one of the following values for each date in the
 *                    publication history:
 *                     * received - date manuscript received for review
 *                     * accepted - accepted for publication
 *                     * revised - article revised by publisher or author
 *                     * aheadofprint - published electronically prior to final
 *                       publication
 *                     * epublish – published electronically
 *                     * ppublish – published in print
 *                     * ecollection – used for electronic-only journals that
 *                       publish individual articles and later collect them into
 *                       an “issue” date, typically called an eCollection.
 *
 *
 */
// function hook_pmc_submit_history($node) {
//   return array(
//     'history' => array(
//       array(
//         'pubdate' => array(
//           'year' => 2018,
//           'month' => '3',
//           // 'season' => 'Spring',
//           'day' => 4,
//         ),
//         'pubstatus' => 'epublish',
//       ),
//     ),
//   );
// }

/**
 * Abstract.
 *
 * SC 04 apr 2018 12:10:12 stefano.
 *
 * @param $node object
 *   The node object.
 *
 * @return array
 *   A multidimensional array with key
 *   - abstract => Some multidimensional array of information about the abstract
 *                 of the article:
 *     - 'lang' [ISO 639] => An multidimensional array for every abstract language.
 *       - abstracttext => The article’s abstract. Include all text as a single
 *                         ASCII paragraph. This element can be used to submit
 *                         structured abstract section headings. [string]
 *       - label => Attribute containing the structured abstract section
 *                  heading. Most used:
 *                   * BACKGROUND
 *                   * OBJECTIVE
 *                   * METHODS
 *                   * RESULTS
 *                   * CONCLUSIONS
 *                  If not present only 1 'abstracttext' can exist.  [string]
 *
 *
 */
function hook_pmc_submit_abstract($node, $node_i18n) {
  return array(
    'abstract' => array(
      'en' => array(
        array(
          'abstracttext' => 'abstract part 1',
          'label' => 'BACKGROUND',
        ),
        array(
          'abstracttext' => 'abstract part 2',
          'label' => 'METHODS',
        ),
      ),
      'it' => array(
        array(
          'abstracttext' => 'abstract completo',
        ),
      ),
    ),
  );
}

/**
 * PubDate.
 *
 * SC 04 apr 2018 18:59:15 stefano.
 *
 * @param $node object
 *   The node object.
 *
 * @return array
 *   A array with key
 *   - journal_pubdate => The publication date information. [timestamp]
 *
 */
function hook_pmc_submit_journal_pubdate($node) {
  return array(
    'journal_pubdate' => 1509120838,
  );
}

/**
 * Object.
 *
 * SC 05 apr 2018 01:06:27 stefano.
 *
 * @param $node object
 *   The node object.
 *
 * @return array
 *   A multidimensional array with key
 *   - object => Some multidimensional array of information about various data.
 *     - type => The identifier of data. See the doc for the full list of
 *               values. [string]
 *     - param => The value of the Object. [string]
 *     - name => Attribute, which may only include the below for the following
 *               values for each param.
 *                * id
 *                * value (only for 'Keywords' type)
 *               [string]
 *
 */
function hook_pmc_submit_object($node) {
  return array(
    'object' => array(
      array(
        'type' => 'Keyword',
        'param' => 'key one',
        'name' => 'value',
      ),
      array(
        'type' => 'Comment',
        'param' => 'comm one',
        'name' => 'id',
      ),
    ),
  );
}

/**
 * The pdf file of the published manuscript.
 *
 * SC 07 mag 2018 12:04:28 stefano.
 *
 * @param $node object
 *   The node object.
 *
 * @return array
 *   A multidimensional array with key
 *   - pdf => An array with key
 *     - fid => The file ID of PDF [int]
 *
 */
function hook_pmc_submit_file_pdf($node) {
  return array(
    'pdf' => array('fid' => 1),
  );
}

/**
 * The estra files added to manuscript.
 *
 * SC 07 mag 2018 12:16:14 stefano.
 *
 * @param $node object
 *   The node object.
 *
 * @return array
 *   A multidimensional array with key
     - files => Some multidimensional array of information about various data.
       - fid => The file ID of attached files to Article [int]
       - type => The type of file. Is optional, and identifies one of the
                 following:
                  * -g figure graphic + alphanumeric identifier
                  * -i inline graphic + alphanumeric identifier
                  * -e equation + alphanumeric identifier
                  * -s supplementary data file + alphanumeric identifier
                 [string]

 */
function hook_pmc_submit_more_file($node) {
  return array(
    'files' => array(
      array(
        'fid' => 2,
        'type' => '-g01',
      ),
      array(
        'fid' => 3,
        'type' => '-g02',
      ),
    ),
  );
}



