<?php
/**
 * @file
 * Module to submit mauscripts to PubMed.
 *
 * Date: 15 mar 2018 12:48:21
 * File: pmc_submit.admin.inc
 * Author: stefano.
 */

/**
 * Implements drupal_get_form()
 *
 * SC 15 mar 2018 18:19:42 stefano.
 */
function _pmc_submit_admin_settings($form, &$form_state) {
  // XML.
  //
  // SC 16 mar 2018 10:36:41 stefano.
  //
  $form['pmc_submit_xml'] = array(
    '#type' => 'fieldset',
    '#title' => t('XML Tags'),
    '#description' => t('The XML static values.'),
    '#weight' => 1,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['pmc_submit_xml']['pmc_submit_xml_header'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('XML Document type'),
    '#description' => t('The Document string that tells us the DTD information.'),
    '#default_value' => variable_get('pmc_submit_xml_header', '<!DOCTYPE ArticleSet PUBLIC "-//NLM//DTD PubMed 2.7//EN" "https://dtd.nlm.nih.gov/ncbi/pubmed/in/PubMed.dtd">'),
    '#weight' => 2,
    '#required' => TRUE,
    // Hardcoded
    //
    // SC 19 mar 2018 17:56:34 stefano.
    //
    '#disabled' => FALSE,
  );

  $form['pmc_submit_xml']['pmc_submit_journal'] = array(
    '#type' => 'fieldset',
    '#title' => t('Citation information about the journal issue'),
    '#weight' => 3,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['pmc_submit_xml']['pmc_submit_journal']['pmc_submit_publishername'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('The publisher name'),
    '#description' => t('The publisher name.'),
    '#default_value' => variable_get('pmc_submit_publishername', ''),
    '#weight' => 4,
    '#required' => TRUE,
  );
  $form['pmc_submit_xml']['pmc_submit_journal']['pmc_submit_journaltitle'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('Journal title'),
    '#description' => t('The NLM Title Abbreviation for the journal. If you do not know the abbreviation, see the NLM Catalog.'),
    '#default_value' => variable_get('pmc_submit_journaltitle', ''),
    '#weight' => 5,
    '#required' => TRUE,
  );
  $form['pmc_submit_xml']['pmc_submit_journal']['pmc_submit_issn'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('ISSN'),
    '#description' => t('The ISSN or ESSN of the journal. An ESSN can be supplied if it exists in the journal\'s NLM Catalog record.'),
    '#default_value' => variable_get('pmc_submit_issn', ''),
    '#weight' => 6,
    '#required' => TRUE,
  );

  $form['pmc_submit_xml']['pmc_submit_copyrightinformation'] = array(
    '#type' => 'textarea',
    '#title' => t('Copyright'),
    '#description' => t('The copyright information associated with the articles.'),
    '#default_value' => variable_get('pmc_submit_copyrightinformation', ''),
    '#weight' => 7,
    '#required' => FALSE,
  );

  $form['pmc_submit_xml']['pmc_submit_coistatement'] = array(
    '#type' => 'textarea',
    '#title' => t('CoI statement'),
    '#description' => t('The Conflict of Interest statement associated with the articles.'),
    '#default_value' => variable_get('pmc_submit_coistatement', ''),
    '#weight' => 8,
    '#required' => FALSE,
  );

  // FTP.
  //
  // SC 16 mar 2018 10:24:05 stefano.
  //
  $form['pmc_submit_ftp'] = array(
    '#type' => 'fieldset',
    '#title' => t('FTP account'),
    '#description' => t('Contact publisher@ncbi.nlm.nih.gov to request FTP login information.'),
    '#weight' => 2,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['pmc_submit_ftp']['pmc_submit_username'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('Username'),
    '#description' => t('Your login name.'),
    '#default_value' => variable_get('pmc_submit_username', ''),
    '#required' => TRUE,
  );

  $form['pmc_submit_ftp']['pmc_submit_password'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('Password'),
    '#description' => t('Your password.'),
    '#default_value' => variable_get('pmc_submit_password', ''),
    '#required' => TRUE,
  );

  $form['pmc_submit_ftp']['pmc_submit_url'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('URL'),
    '#description' => t('The ftp URL. The URL is absolute (beginning with a scheme such as "ftp:").'),
    '#default_value' => variable_get('pmc_submit_url', ''),
    '#required' => TRUE,
  );

  // Node.
  //
  // SC 16 mar 2018 10:35:44 stefano.
  //
  $form['pmc_submit_node'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node'),
    '#description' => t('Options related to node.'),
    '#weight' => 3,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['pmc_submit_node']['pmc_submit_ntype'] = array(
    '#type' => 'select',
    '#title' => t('Content type'),
    '#options' => node_type_get_names(),
    '#description' => t('The manuscript content type ready for PubMed.'),
    '#default_value' => variable_get('pmc_submit_ntype', array()),
    '#multiple' => TRUE,
    '#required' => TRUE,
  );

  // Sandbox test area.
  //
  // SC 12 set 2018 19:44:49 stefano.
  //
  $form['pmc_submit_ftp']['pmc_submit_sandbox'] = array(
    '#type' => 'fieldset',
    '#title' => t('FTP sandbox'),
    '#description' => t('To submit a sample XML file.'),
    '#weight' => 2,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['pmc_submit_ftp']['pmc_submit_sandbox']['pmc_submit_sandbox_check'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sandbox'),
    '#description' => t('Active the test sample.'),
    '#default_value' => variable_get('pmc_submit_sandbox_check', ''),
//     '#required' => TRUE,
  );

  $sandbox_condition = array(
    array(
      ':input[name="pmc_submit_sandbox_check"]' => array('checked' => TRUE),
    ),
  );


  $form['pmc_submit_ftp']['pmc_submit_sandbox']['pmc_submit_sandbox_email'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('Email'),
    '#description' => t('The NCBI address to send a notification.'),
    '#default_value' => variable_get('pmc_submit_sandbox_email', ''),
    '#states' => array('required' => $sandbox_condition),
  );

  $form['pmc_submit_ftp']['pmc_submit_sandbox']['pmc_submit_sandbox_url'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('URL'),
    '#description' => t('The ftp URL. The URL is absolute (beginning with a scheme such as "ftp:").'),
    '#default_value' => variable_get('pmc_submit_sandbox_url', ''),
    '#states' => array('required' => $sandbox_condition),
  );


  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 99,
  );

  return $form;
}

function _pmc_submit_admin_settings_submit($form, &$form_state) {
  // XML.
  //
  // SC 16 mar 2018 10:18:28 stefano.
  //
  variable_set('pmc_submit_xml_header', $form_state['values']['pmc_submit_xml_header']);
  variable_set('pmc_submit_publishername', $form_state['values']['pmc_submit_publishername']);
  variable_set('pmc_submit_journaltitle', $form_state['values']['pmc_submit_journaltitle']);
  variable_set('pmc_submit_issn', $form_state['values']['pmc_submit_issn']);
  variable_set('pmc_submit_copyrightinformation', $form_state['values']['pmc_submit_copyrightinformation']);
  variable_set('pmc_submit_coistatement', $form_state['values']['pmc_submit_coistatement']);

  // FTP.
  //
  // SC 16 mar 2018 10:18:45 stefano.
  //
  variable_set('pmc_submit_username', $form_state['values']['pmc_submit_username']);
  variable_set('pmc_submit_password', $form_state['values']['pmc_submit_password']);
  variable_set('pmc_submit_url', $form_state['values']['pmc_submit_url']);

  // Node.
  //
  // SC 16 mar 2018 10:36:08 stefano.
  //
  variable_set('pmc_submit_ntype', $form_state['values']['pmc_submit_ntype']);

  // Sandbox
  //
  // SC 12 set 2018 19:48:55 stefano.
  //
  variable_set('pmc_submit_sandbox_check', $form_state['values']['pmc_submit_sandbox_check']);
  variable_set('pmc_submit_sandbox_url', $form_state['values']['pmc_submit_sandbox_url']);
  variable_set('pmc_submit_sandbox_email', $form_state['values']['pmc_submit_sandbox_email']);

}

function _pmc_submit_admin_settings_validate($form, &$form_state) {

  if (!valid_url($form_state['values']['pmc_submit_url'], TRUE)) {
    form_set_error('pmc_submit_url', t('The URL is formatted bad.'));
  }

  if (!preg_match("|^<!DOCTYPE.+>$|", $form_state['values']['pmc_submit_xml_header'])) {
    form_set_error('pmc_submit_xml_header', t('Please, insert the full document type string.'));
  }

  // Sandbox
  //
  // SC 12 set 2018 19:45:37 stefano.
  //
  if ($form_state['values']['pmc_submit_sandbox_check']) {
    if (empty($form_state['values']['pmc_submit_sandbox_url'])) {
      form_set_error('pmc_submit_sandbox_url', t('The sandbox URL is required in sandbox.'));
    }
    elseif (!valid_url($form_state['values']['pmc_submit_sandbox_url'], TRUE)) {
      form_set_error('pmc_submit_sandbox_url', t('The sandbox URL is formatted bad.'));
    }

    if (empty($form_state['values']['pmc_submit_sandbox_email'])) {
      form_set_error('pmc_submit_sandbox_email', t('The email is required in sandbox.'));
    }
    elseif (!valid_email_address($form_state['values']['pmc_submit_sandbox_email'], TRUE)) {
      form_set_error('pmc_submit_sandbox_email', t('The email is formatted bad.'));
    }
  }

}

