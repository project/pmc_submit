/**
 * @file
 * 
 *
 * Date: 11 apr 2018 11:42:39
 * File: pcm_submit.js
 * Author: stefano
 */

/*
#pmc_submit_nbci_iframe div.wrap div.page div {
  display: none;
}

#pmc_submit_nbci_iframe div.wrap div.page div.container {
  display: block;
}
*/
/*jQuery(document).ready(function($) {
  
  var f = $('#pmc_submit_nbci_iframe');
  
  f.load(function(){
    f.contents().find('div.wrap div.page div').hide(); 
    f.contents().find('div.wrap div.page div.container').show(); 
    f.contents().find('div.wrap div.page div.container div').show(); 
  })

  f.ready(function(){
    f.contents().find('body').css('background-color', '');
  })
});*/

(function ($) {
  Drupal.behaviors.pmc_submit = {
    attach: function (context, settings) {
      // Code to be run on page load, and
      // on ajax load added here
      var nbci = $('#pmc_submit_nbci');
      var pmcs = $('#pmc_textarea_bg');
      
      nbci.find('div#maincontent p.info a').hide(); 
      nbci.find('table.errlist tbody tr').on("click", pmc_nbci_error);
      
      function pmc_nbci_error() {
        
        var line_error = this.dataset.line;
        
        pmc_handle_input();
        
        $('div#pmc_textarea_bg_line_' + line_error ).addClass('pmc_submit_xml_line_error');
      }
        
      /** SC 01 mag 2018 17:43:35
       * Highlight textarea row
       */
      var pmc_textarea = $('#pmc_textarea_fg textarea');
      var pmc_textarea_padding_t = pmc_textarea.css('padding-top')
      var pmc_textarea_padding_r = pmc_textarea.css('padding-right')
      var pmc_textarea_padding_b = pmc_textarea.css('padding-bottom')
      var pmc_textarea_padding_l = pmc_textarea.css('padding-left')
      
      /** SC 01 mag 2018 18:38:39
       * Resize the div like the textarea
       */
      var resizeInt = null;
     
      var resizeEvent = function() {
        pmcs.width(pmc_textarea.innerWidth() - (parseFloat(pmc_textarea_padding_l) || 0) - (parseFloat(pmc_textarea_padding_r) || 0));
        pmcs.height(pmc_textarea.innerHeight() - (parseFloat(pmc_textarea_padding_t) || 0) - (parseFloat(pmc_textarea_padding_b) || 0));
      };
      
      resizeEvent();
      
//      pmcs.css('padding-top', pmc_textarea_padding_t);
//      pmcs.css('padding-right', pmc_textarea_padding_r);
//      pmcs.css('padding-bottom', pmc_textarea_padding_b);
//      pmcs.css('padding-left', pmc_textarea_padding_l);
      
      
      pmc_textarea.on({
        'input' : pmc_handle_input,
        'scroll' : pmc_handle_scroll,
        'mousedown' : pmc_handle_resize,
      });
      
      // The mouseup event stops the interval,
      // then call the resize event one last time.
      // We listen for the whole window because in some cases,
      // the mouse pointer may be on the outside of the textarea.
      $(window).on("mouseup", function(e) {
          if (resizeInt !== null) {
              clearInterval(resizeInt);
          }
          resizeEvent();
      });

      function pmc_handle_input() {
        pmcs.find('.pmc_submit_xml_line').removeClass('pmc_submit_xml_line_error');
      }

      function pmc_handle_scroll() {
//        pmcs.find('.pmc_submit_xml_line').css('background-color', 'yellow');
        
        let scrollTop = pmc_textarea.scrollTop();
        pmcs.scrollTop(scrollTop);

        // Chrome and Safari won't break long strings of spaces, which can cause
        // horizontal scrolling, this compensates by shifting highlights by the
        // horizontally scrolled amount to keep things aligned
        let scrollLeft = pmc_textarea.scrollLeft();
        pmcs.css('transform', (scrollLeft > 0) ? 'translateX(' + -scrollLeft + 'px)' : '');

      }

      function pmc_handle_resize() {
        pmcs.find('.pmc_submit_xml_line').css('color', 'blu');
        resizeInt = setInterval(resizeEvent, 1000/15);
      }

    }
  };
}(jQuery));

