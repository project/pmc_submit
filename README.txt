PubMed submit

DESCRIPTION
-----------

PubMed comprises over 28 million citations for biomedical literature from 
MEDLINE, life science journals, and online books. PubMed citations and 
abstracts include the fields of biomedicine and health, covering portions of 
the life sciences, behavioral sciences, chemical sciences, and bioengineering. 
PubMed also provides access to additional relevant web sites and links to the 
other NCBI molecular biology resources.
PubMed is a free resource that is developed and maintained by the National 
Center for Biotechnology Information (NCBI), at the U.S. National Library of 
Medicine (NLM), located at the National Institutes of Health (NIH).
Publishers of journals in PubMed must submit citation and abstract data 
electronically. PubMed only accepts citation and abstract data uploaded by 
File Transfer Protocol (FTP) that is in the PubMed XML tagged format. FTP 
accounts are provided for publishers to send data in a confidential and 
reliable manner. These citations are then added to PubMed, and PubMed Unique 
Identifiers (PMIDs) are returned to the publisher.

This module generates, from a node/fields, the XML file ready to upload in 
the PubMed database.

REQUIREMENTS
------------

None from Drupal.

The PubMed requires an own account. 

INSTALLATION
------------

 1. CREATE DIRECTORY

    Create a new directory "pmc_submit" in the sites/all/modules directory or 
    everywhere you install your modules and place the entire contents of this 
    module in it.

 2. ENABLE THE MODULE

    Enable the module on the Modules admin page.

 3. ACCESS PERMISSION

    Grant the access at the Access control page:
      People > Permissions.

 4. CONFIGURE THE MODULE
 
    

CONFIGURATION
-------------

 1. Go to admin/config/services/pmc_submit and fill the fields:

    XML TAGS
      Under "XML Document type" insert the Document string that tells us the 
      DTD information (leave the default unless you know what you do).
     
      Under "Citation information about the journal issue" insert:
      - the publisher name;
      - the NLM Title Abbreviation for the journal. If you do not know the 
        abbreviation, see the NLM Catalog;
      - the ISSN of the journal;
      - the copyright information associated with the articles;
      - the Conflict of Interest statement associated with the articles.
      
    FTP ACCOUNT
      Insert your login name, password and the FTP url from you account on 
      PubMed.
      If you need, also fill the fields in the FTP sandbox section and check-it 
      to activate.
    
    NODE
      Select one or more of the manuscript/node content type ready for PubMed. 
      
      We recomend to create a new content type. A lot of fields are needed to 
      add for PubMed.
    
 2. API
 
    The module uses a set of API (hooks) to retrieve the data for the XML. 
    Implements your hook functions (only some are required).

